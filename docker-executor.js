//
// Docker executor
//
// The docker executor restarts the deployed repo in a dedicated docker container according to repo parameters

var util = require('util');
var Docker = require('dockerode');
var dockerOptions = {};

if (process.env.DOCKER_PORT) {
  dockerOptions.port = process.env.DOCKER_PORT;
  dockerOptions.host = 'localhost';
}
else {
  dockerOptions.socketPath = '/var/run/docker.sock';
}

var docker = new Docker(dockerOptions);

var GRACE_PERIOD = 5000; // allow a grace period of 5sec before shutdown
var NAME_SEPARATOR = '.';
var START_RETRYS = 3;

exports.preDeploy = function(repo, cb) {
  var containerName = repo.name;
  docker.listContainers(function (err, containers) {
    if (err) {
      console.error('Failed to list containers', err);
      return cb(err);
    }

    var pending = repo.pending = [];

    // find all containers that starts with the same name and mark them for removal
    containers.forEach(function (containerInfo) {
      if (containerInfo.Names && containerInfo.Names.length) {
        var filterByName = containerInfo.Names.filter(function(name) { 
          // container names apear as /xxx. check if there is a container that start with /:containerName
          var parts = name.split('/');
          // there should be exactly one '/' char else the name is of a linked container
          if (parts.length === 2) {
            // drop the timestamp
            parts = parts[1].split(NAME_SEPARATOR);
            parts.pop();

            return parts.join(NAME_SEPARATOR) === containerName;
          }
        });
        if (filterByName.length) {
          console.log('Found an existing container with the same name: %s.\nAdding container to list.', filterByName[0]);
          return pending.push(containerInfo.Id);
        }
      }
    });

    return cb();
  });  
};

function sendSignal(pending, signal, cb) {
  if (typeof timeOutInterval === 'function') {
    cb = signal;
    signal = 'SIGHUP';
  }
  cb = cb || function() {};

  pending = pending || [];

  var count = pending.length;
  
  if (!count) {
    return cb(pending.length);
  }

  function onDone() {
    count--;
    if (count <= 0) {
      return cb(pending.length);
    }
  }

  pending.forEach(function(containerId) {
    var container = docker.getContainer(containerId);

    console.log('Signaling container: %s', containerId);
    container.kill({signal: signal}, function(err) {
      if (err) {
        console.error('Failed to stop %s', containerId, err);
      }

      return onDone();
    });

  });
}

function removePendingContainers(pending, timeOutInterval, cb) {
  if (typeof timeOutInterval === 'function') {
    cb = timeOutInterval;
    timeOutInterval = undefined;
  }

  cb = cb || function() {};

  pending = pending || [];

  var count = pending.length;
  
  if (!count) {
    return cb();
  }
  
  function onRemove() {
    count--;
    if (count <= 0) {
      return cb();
    }
  }

  pending.forEach(function(containerId) {
    var container = docker.getContainer(containerId);

    function removeContainer() {
      // stop any previous repo docker
      console.log('Stopping and removing container: %s', containerId);
      container.stop(function(err) {
        if (err) {
          console.error('Failed to stop %s', containerId, err);
          return onRemove();
        }

        // remove it, we will recreate it upon successful deployment
        container.remove(function(err) {
          if (err) {
            console.error(err);
            return onRemove();
          }

          console.log('%s removed', containerId);
          return onRemove();
        });    
      });
    }

    if (timeOutInterval) {
      return setTimeout(removeContainer, timeOutInterval);
    }

    return removeContainer();
  });
}

exports.start = function(repo, username, branch) {
  var createOptions = repo.executor.create_options || {};
  createOptions.options = createOptions.options || {};
  createOptions.HostConfig = createOptions.HostConfig || {};
  createOptions.HostConfig.Binds = createOptions.HostConfig.Binds || [];
  createOptions.Env = createOptions.Env || [];

  // set the container name to the repo name
  //createOptions.options.name = repo.name + NAME_SEPARATOR + Date.now();
  var name = repo.name + NAME_SEPARATOR + Date.now();

  var normalizedDeployPath = repo.deployPath.replace(/\\/g, '/');

  if (!repo.skipBinds) {
    // mount the deploy dir to this container as /app
    var binding = normalizedDeployPath + ':/app';
    createOptions.HostConfig.Binds.push(binding);
  }
  else {
    createOptions.Env.push('NO_APP_BIND=true');
  }

  createOptions.Env.push(util.format('APP_PATH=%s', normalizedDeployPath));
  if (username) {
    createOptions.Env.push(util.format('PUSH_USERNAME=%s', username));
  }
  if (branch) {
    createOptions.Env.push(util.format('GIT_BRANCH=%s', branch));
  }

  createOptions.name = name;

    return docker.createContainer(createOptions, function (err, container) {
      if (err) {
        return console.error('Failed to create container:', err);
      }

      console.log('Create result: ', container);
      (function startContainer(retryCount, err) {
        if (retryCount <= 0) {
          return console.error('Failed to create container.', err);
        }

        container.start(repo.executor.start_options, function (err, data) {
          if (err) {
            console.error('Failed to start container retrying in %dms. Error:', GRACE_PERIOD, err);
            return setTimeout(function() {startContainer(--retryCount, err);}, GRACE_PERIOD);
          }

          return console.log('Running %s:', repo.name, data, container);
        });
      })(START_RETRYS);
    });
};

exports.restart = function(repo, username, branch) {
  if (repo.useSignal) {
    return sendSignal(repo.pending, repo.useSignal, function(count) {
      if (count > 0) {
        return console.log('Sent %s signal to matching containers', repo.useSignal);
      }

      return exports.start(repo, username, branch);
    });
  }

  if (repo.removeOldFirst) {
    return removePendingContainers(repo.pending, function() {
      if (repo.removeWaitPeriod) {
        console.log('Waiting %dms before starting %s', repo.removeWaitPeriod, repo.name);
        return setTimeout(function() {
          exports.start(repo, username, branch);
        }, repo.removeWaitPeriod);
      }
      
      return exports.start(repo, username, branch);
    });
  }

  exports.start(repo, username, branch);

  return removePendingContainers(repo.pending, GRACE_PERIOD);
};
